<!-- Bootstrap Modal Survey -->
<div id="modal-questionExtraSurvey" class="modal"  tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <div class="h4 modal-title"></div>
      </div>
      <div class="modal-body">
        <iframe name="frame-questionExtraSurvey" class="extra-survey"></iframe></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-survey" data-action="saveall">Save</button>
        <button type="button" class="btn btn-success btn-survey" data-action="movesubmit">Submit</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
